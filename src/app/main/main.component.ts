import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';

declare var $;
declare var alertify;

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit {
  deportes = [
    "Fútbol",
    "Ciclismo",
    "Tenis",
    "Baloncesto",
    "Balonmano",
    "Atletismo",
    "Voleibol",
    "Ajedrez",
    "Automovilismo",
    "Pesca",
    "Caza",
    "Pelota - mano",
    "Escalada",
    "Senderismo",
    "Natación",
    "Golf",
    "Karate",
    "Judo",
    "Badminton",
    "Hípica",
    "Gimnasia",
    "Rugby",
    "Esgrima",
    "Triatlón",
    "Padel",
    "Boxeo",
    "Patinaje",
    "Piragüismo",
    "Billar",
    "Tiro",
    "BTT",
  ];

  filter = {
    fFundacion: {
      desde: null,
      hasta: null,
    },
    fInscripcion: {
      desde: null,
      hasta: null,
    },
    deportes: [],
  };

  loaded = false;
  data = [];
  tresD = false;

  constructor(private dataService: DataService) {}

  ngOnInit(): void {
    $('#select2Deportes')
      .select2()
      .on('select2:select', (e) => {
        let deporte = e.params.data.text;
        let exist = this.filter.deportes.find((d) => d == deporte);
        if (!exist) this.filter.deportes.push(deporte);
      })
      .on('select2:unselect', (e) => {
        let deporte = e.params.data.text;
        let exist = this.filter.deportes.find((d) => d == deporte);
        if (exist) {
          let index = this.filter.deportes.indexOf(deporte);
          this.filter.deportes.splice(index, 1);
        }
      });
    this.dataService.getData().subscribe(
      (result) => {
        let list = result.split('\n');
        for (let item of list) {
          let arr = item.split(';');
          let val = {
            registro: arr[0],
            nombre: arr[1],
            domicilio: arr[2],
            provincia: arr[3],
            localidad: arr[4],
            cod_postal: arr[5],
            tel: arr[6],
            fax: arr[7],
            email: arr[8],
            web: arr[9],
            f_fundacion: arr[10],
            f_inscripcion: arr[11],
            deportes: arr[12],
          };
          this.data.push(val);
        }
        this.loaded = true;
        alertify.success('Se han obtenido los datos correctamente.', 2);
      },
      (error) => {
        alertify.error('Se ha producido un error al obtener los datos.', 2);
        this.loaded = true;
        return;
      }
    );
  }

  filterData() {
    let date = new Date().toISOString().split('T')[0];
    if (this.filter.fFundacion.desde > this.filter.fFundacion.hasta) {
      alertify.error(
        'La fecha del campo "hasta" debe ser mayor que la fecha del campo "desde".'
      );
      return;
    }
    if (this.filter.fInscripcion.desde > this.filter.fInscripcion.hasta) {
      alertify.error(
        'La fecha del campo "hasta" debe ser mayor que la fecha del campo "desde".'
      );
      return;
    }
    if (
      this.filter.fInscripcion.desde > date ||
      this.filter.fFundacion.desde > date
    ) {
      alertify.error(
        'La fecha del campo "desde" debe ser menor que la fecha actual.'
      );
      return;
    }
    if(window.innerWidth <= 768) {
      this.toogleSidebar();
    } 
    this.loaded = false;

    this.data = [];
    this.dataService.getData().subscribe(
      (result) => {
        let list = result.split('\n');
        for (let item of list) {
          let arr = item.split(';');
          let val = {
            registro: arr[0],
            nombre: arr[1],
            domicilio: arr[2],
            provincia: arr[3],
            localidad: arr[4],
            cod_postal: arr[5],
            tel: arr[6],
            fax: arr[7],
            email: arr[8],
            web: arr[9],
            f_fundacion: arr[10],
            f_inscripcion: arr[11],
            deportes: arr[12],
          };
          if(this.addVal(val)) this.data.push(val);
        }
        this.loaded = true;
        if (this.data && this.data.length == 0) {
          alertify.error(
            'No se han encontrado datos con los filtros indicados.',
            2
          );
        } else if (this.data && this.data.length != 0) {
          alertify.success('Datos filtrados correctamente.', 2);
        }
      },
      (error) => {
        alertify.error('Se ha producido un error al obtener los datos.', 2);
        this.loaded = true;
        return;
      }
    );
  }

  toogleSidebar() {
    if ($('.sidebar').hasClass('notVisible')) {
      $('.sidebar').removeClass('notVisible');
      $('.sidebar').fadeIn('slow');
    } else {
      $('.sidebar').toggle('slow');
    }
  }

  addVal(val) {
    let ffh
    let ffd;
    let fid;
    let fih;
    let ff;
    let fi;
    ffh = this.setDate(this.filter.fFundacion.hasta);
    ffd = this.setDate(this.filter.fFundacion.desde);
    fid = this.setDate(this.filter.fInscripcion.desde);
    fih = this.setDate(this.filter.fInscripcion.hasta);
    ff = this.setDate(val.f_fundacion);
    fi = this.setDate(val.f_inscripcion);
    let arrDep = [];
    if(this.filter.deportes.length != 0){
      for (let deporte of this.filter.deportes) {
        if(val.deportes.toUpperCase().includes(deporte.trim().toUpperCase())) {
          arrDep.push(deporte);
          break;
        }
      }
      if (arrDep.length == 0) return false;
    }
    if(!ff && (ffh || ffd)) return false;
    if(!fi && (fih || fid)) return false;
    if(ffh && (ff > ffh)) return false;
    if(fih && (fi > fih)) return false;
    if(ffd && (ff < ffd)) return false;
    if(fid && (fi < fid)) return false;
    return true;
  }

  setDate(d){
    if(d == null || d == '') return null;
    if(d.includes('-')){
      d = d.split('-');
      d = d[1]+'/'+d[2]+'/'+d[0];
    } else {
      d = d.split('/');
      d = d[1]+'/'+d[0]+'/'+d[2];
    }
    return new Date(d);
  }
}
