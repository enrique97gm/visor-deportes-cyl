import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { DataService } from './services/data.service';
import { HttpClientModule } from '@angular/common/http';
import { MapComponent } from './map/map.component';
import { Map3DComponent } from './map3-d/map3-d.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    MapComponent,
    Map3DComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
