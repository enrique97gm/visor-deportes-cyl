import { Component, Input, OnInit } from '@angular/core';
import * as L from 'leaflet';

declare var $;

@Component({
  selector: 'map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements OnInit {
  @Input() data;
  map;
  max;
  min;
  leyenda = {};
  mapTile = null;
  selectedProvince = null;
  selectedData = [];

  constructor() {}

  ngOnInit(): void {
    setTimeout(() => {
      this.getMaxAndMin();
      this.initMap();
    }, 10);
  }

  initMap() {
    this.map = L.map('map', {
      center: [41.74410425169406, -4.504280880482643],
      zoom: 7,
    });

    $.ajax('./assets/provincias-CyL.geojson').done((data) => {
      try {
        data = JSON.parse(data);
      } catch (e) {}
      let geoJson = L.geoJSON(data, {
        onEachFeature: (feature, featureLayer) => {
          featureLayer.on('click', () => {
            this.selectedData = this.data.filter(
              (d) => d.provincia == feature.properties.provincia
            );
            this.selectedProvince = feature.properties.provincia;
            setTimeout(() => {
              $('#table').DataTable({
                language: {
                  url: 'https://cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json',
                },
              });
            }, 0);
          });
        },
      });
      geoJson.setStyle((feature) => {
        return {
          fillColor: this.getProvinceColor(feature.properties.texto),
          fillOpacity: 1,
        };
      });
      geoJson.addTo(this.map);
    });
  }

  closeModalAndResetData(){
    this.selectedData = [];
    this.selectedProvince = null;
  }

  getProvinceColor(province) {
    let arr = this.data.filter((d) => d.provincia == province);
    let color = this.getColor(arr.length);
    this.leyenda[province] = {
      value: arr.length,
      color: color,
    };
    return color;
  }

  getColor(value) {
    let percent = (value - this.min) / (this.max - this.min);
    var hue = (80 - 20) * (1 - percent) + 20;
    return ['hsl(220,100%,', hue, '%)'].join('');
  }

  getMaxAndMin() {
    let provinces = [
      'Ávila',
      'Burgos',
      'León',
      'Palencia',
      'Valladolid',
      'Salamanca',
      'Zamora',
      'Segovia',
      'Soria',
    ];
    this.max = 0;
    this.min = this.data.length;
    for (let p of provinces) {
      let arr = this.data.filter((d) => d.provincia == p);
      if (arr.length > this.max) this.max = arr.length;
      if (arr.length < this.min) this.min = arr.length;
    }
  }

  toogleMapTiles() {
    if (this.mapTile != null) {
      this.map.removeLayer(this.mapTile);
      this.mapTile = null;
    } else {
      this.mapTile = L.tileLayer(
        'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
        {
          maxZoom: 20,
          attribution:
            '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
        }
      );
      this.mapTile.addTo(this.map);
    }
  }
}
