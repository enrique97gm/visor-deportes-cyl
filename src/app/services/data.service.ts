import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(
    private http: HttpClient,
  ) {}

  api = environment.api;

  getData(){
    return this.http.get('./assets/CYA.csv', {responseType: 'text'});
  }
}
