import { Component, Input, OnInit } from '@angular/core';
import * as L from 'leaflet';

declare var Cesium: any;
declare var $;

@Component({
  selector: 'map3D',
  templateUrl: './map3-d.component.html',
  styleUrls: ['./map3-d.component.scss'],
})
export class Map3DComponent implements OnInit {
  @Input() data;
  viewer;
  max;
  min;
  leyenda = {};
  colors = [];
  distance = 700000;

  constructor() {}

  ngOnInit() {
    this.getMaxAndMin()
    this.getColors()
    this.viewer = new Cesium.Viewer('map', {
      contextOptions: {
        webgl: {
          alpha: true,
          depth: false,
          stencil: true,
          antialias: true,
          premultipliedAlpha: true,
          preserveDrawingBuffer: true,
          failIfMajorPerformanceCaveat: true,
        },
        allowTextureFilterAnisotropic: true,
      },
      animation: false, // Whether to display animation controls
      shouldAnimate: true,
      homeButton: false, // Whether to display the Home button
      fullscreenButton: false, // Whether to display the full screen button
      baseLayerPicker: false, // Whether to display the layer selection control
      geocoder: false, // Whether to display the place name search control
      timeline: false, // Whether to display the timeline control
      sceneModePicker: false, // Whether to display the projection mode control
      navigationHelpButton: false, // Whether to display the help information control
      infoBox: false, // Whether to display the information displayed after clicking the element
      requestRenderMode: true, // enable request rendering mode
      scene3DOnly: false, // Each geometry instance will only be rendered in 3D to save GPU memory
      sceneMode: 3, // Initial scene mode 1 2D mode 2 2D loop mode 3 3D mode Cesium.SceneMode
      fullscreenElement: document.body, // HTML elements rendered in full screen are temporarily useless
      selectionIndicator: false,
      imageryProvider: new Cesium.WebMapTileServiceImageryProvider({
        // tslint:disable-next-line: max-line-length
        url:
          'https://t0.tianditu.gov.cn/img_w/wmts?service=WMTS&version=1.0.0&request=GetTile&tilematrixset=w&tk=4d0a3b94af2cf3fd83f5dfb34c81d1f7',
        layer: 'img',
        style: 'default',
        format: 'image/jpeg',
        tileMatrixSetID: 'w',
        credit: new Cesium.Credit('Sky Map Global Image Service'),
        show: false,
        maximumLevel: 18,
      }),
    });
    this.viewer.camera.flyTo({
      destination: Cesium.Cartesian3.fromDegrees(
        -5.04280880482643,
        38.24410425169406,
        this.distance
      ),
      orientation: {
        pitch: Cesium.Math.toRadians(-60.0),
      },
    });
    this.viewer.cesiumWidget.creditContainer.style.display = 'none';
    var promise = Cesium.GeoJsonDataSource.load(
      './assets/provincias-CyL.geojson'
    );
    promise.then((dataSource) => {
      this.viewer.dataSources.add(dataSource);
      var colorHash = {};
      var entities = dataSource.entities.values;
      for (var i = 0; i < entities.length; i++) {
        var entity = entities[i];
        var name = entity.properties.provincia['_value'];
        var color = colorHash[name];
        if (!color) {
          var item = this.colors.find((c) => c.province == name);
          color = Cesium.Color.fromCssColorString(item.color);
          colorHash[name] = color;
        }
        entity.polygon.material = color;
        entity.polygon.outlineColor = Cesium.Color.DARKBLUE;
        entity.polygon.extrudedHeight = 75000 * item.num;;
      }
    });
  }

  async getColors() {
    let provinces = [
      'Ávila',
      'Burgos',
      'León',
      'Palencia',
      'Valladolid',
      'Salamanca',
      'Zamora',
      'Segovia',
      'Soria',
    ];
    for(let p of provinces) {
      let arr = this.data.filter((d) => d.provincia == p);
      let percent = (arr.length - this.min) / (this.max - this.min);
      var hue = (80 - 20) * (1 - percent) + 20;
      let color = {
        color: ['hsl(220,100%,', hue, '%)'].join(''),
        province: p,
        num: percent
      };
      this.leyenda[p] = {
        value: arr.length,
        color: ['hsl(220,100%,', hue, '%)'].join(''),
      };
      this.colors.push(color);
    }
  }

  async getMaxAndMin() {
    let provinces = [
      'Ávila',
      'Burgos',
      'León',
      'Palencia',
      'Valladolid',
      'Salamanca',
      'Zamora',
      'Segovia',
      'Soria',
    ];
    this.max = 0;
    this.min = this.data.length;
    for (let p of provinces) {
      let arr = this.data.filter((d) => d.provincia == p);
      if (arr.length > this.max) this.max = arr.length;
      if (arr.length < this.min) this.min = arr.length;
    }
  }

  setDistance(val){
    console.log(this.distance)
    if(val == 1){
      this.distance = this.distance - 150000;
      this.viewer.camera.flyTo({
        destination: Cesium.Cartesian3.fromDegrees(
          -5.04280880482643,
          38.24410425169406,
          this.distance
        ),
        orientation: {
          pitch: Cesium.Math.toRadians(-60.0),
        },
      });
    } else {
      this.distance = this.distance + 150000;
      this.viewer.camera.flyTo({
        destination: Cesium.Cartesian3.fromDegrees(
          -5.04280880482643,
          38.24410425169406,
          this.distance
        ),
        orientation: {
          pitch: Cesium.Math.toRadians(-60.0),
        },
      });
    }
  }
}
