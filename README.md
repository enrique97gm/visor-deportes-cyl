# Visor - Clubes deportivos de Castilla y Leon

Project developed with angular using Leaflet and Cesium to show data of registered sports clubs in Castilla y Leon

The project is accesible at the following link: https://enrique97gm.gitlab.io/visor-deportes-cyl

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.2.0.

The git project doesn´t have Node's dependencies. Run `npm install` to install all dependencies.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
